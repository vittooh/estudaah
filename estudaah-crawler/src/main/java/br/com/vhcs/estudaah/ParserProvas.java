package br.com.vhcs.estudaah;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.bson.Document;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by vitor on 26/02/18
 */
public class ParserProvas implements Parser {

    private static final String PROVA = "PROVA";
    private static final String A = "A) ";
    private static final String B = "B) ";
    private static final String C = "C) ";
    private static final String D = "D) ";
    private static final String E = "E) ";
    private static final String PROCESSO = "Processo";
    private static final String QUESTÃO = "QUESTÃO";
    private static final String QUESTION = "QUESTION";
    private static final String AREA = "AREA";
    private static final String ALTERNATIVAS = "ALTERNATIVAS";
    private static final String CONTEUDO = "CONTEUDO";
    private static final String PÁGINA = "Página";
    private static final String pathProblema = "/home/vitor/Desenvolvimento/PocsTCC/provasUFU/provas-com-problemas/";
    public static final String HABILIDADES_ESPECÍFI_CAS = "Habilidades Específi cas";
    public static List<String> blacklist;

    public void processaProvas(File diretorio, String pathBlacklist) {
        loadBlacklist(pathBlacklist);
        for (File currentFolder : diretorio.listFiles()) {
            String erro = retornaPath(currentFolder.getAbsolutePath(), true);
            String corretas = retornaPath(currentFolder.getAbsolutePath(), false);
            if (erro == null || corretas == null) {
                System.out.println("Pastas de erro ou corretas não foram geradas!!");
                return;
            }
            for (File f : currentFolder.listFiles()) {
                if (blacklist.contains(f.getName())) {
                    System.out.println("BLACKLISTER Skipando ::  " + f.getName());
                    continue;
                }
                if (f.getName().contains("Prova") && !f.getName().contains("Redação")) {
                    PDDocument doc = null;

                    try {
                        List<String> conteudoProva;
                        if (f.getName().endsWith(".pdf")) {
                            doc = PDDocument.load(f);
                            PDFTextStripper pdfText = new PDFTextStripper();
                            pdfText.setSortByPosition(true);
                            String text = pdfText.getText(doc);
                            if (!text.contains("OBJETIVA") && !f.getName().contains("OBJETIVA"))
                                continue;
                            conteudoProva = Arrays.asList(text.split("\n"));
                        } else {
                            conteudoProva = Files.readAllLines(Paths.get(f.getPath()), StandardCharsets.ISO_8859_1);
                        }


                        System.out.println("Processando prova " + f.getName());
                        processaObjetivas(conteudoProva, f.getName());
                        boolean result = f.renameTo(new File(corretas + "/" + f.getName()));
                        System.out.println("Movendo arquivo para pasta de sucesso resultado foi:: " + result);

                    } catch (Throwable t) {
                        System.out.println("algum problema ocorreu parsando a prova " + f.getName() + "\nmovendo para o diretorio de provas com problemas!!");
                        t.printStackTrace();
                        f.renameTo(new File(erro + "/" + f.getName()));
                    }
                }
            }
        }
    }


    @Override
    public void processaObjetivas(List<String> linhas, String nomeProva) {

        List<Document> questoes = new ArrayList<>();
        final Document[] questao = {new Document()};
        AtomicBoolean acheiAlternativa = new AtomicBoolean(false);
        String header = "Processo Seletivo/UFU";
        final String[] lastTopic = {""};
        AtomicBoolean lastLineheader = new AtomicBoolean(false);
        linhas.forEach(linha -> {
            if (!linha.contains(header)) {
                if (linha.contains(QUESTÃO) || linha.contains(QUESTION)) {
                    acheiAlternativa.set(false);
                    lastLineheader.set(false);
                    questao[0] = persiteEReseta(questoes, questao[0]);
                    trataQuestao(linha, questao[0]);
                    questao[0].put(PROVA, nomeProva);
                } else if (lastLineheader.get()) {
                    lastLineheader.set(false);
                    if (lastTopic[0].isEmpty()) {
                        lastTopic[0] = linha;
                    } else if (!lastTopic[0].equals(linha)) {
                        questao[0] = persiteEReseta(questoes, questao[0]);
                        lastTopic[0] = linha;
                    }
                } else {
                    if (!ehAlternativa(linha) && !acheiAlternativa.get()) {
                        trataConteudoPergunta(linha, questao[0]);
                    } else {
                        acheiAlternativa.set(true);
                        trataAlternativas(linha, questao[0]);
                    }

                }
            } else {
                lastLineheader.set(true);
            }


        });

        persiteEReseta(questoes, questao[0]);

        MongoClient mc = new MongoClient();
        MongoDatabase db = mc.getDatabase("search-provas-ufu");
        MongoCollection<Document> collection = db.getCollection("provas");

        collection.insertMany(questoes);

    }

    private Document persiteEReseta(List<Document> questoes, Document query) {
        if (query.size() > 1) {
            try {
                query.append(CONTEUDO, query.get(CONTEUDO, StringBuilder.class).toString());
                questoes.add(query);
                query = new Document();
                iniciaAlternativas(query);
            } catch (NullPointerException n) {
                System.out.println();
            }

        }

        return query;
    }

    private void trataQuestao(String linha, Document questao) {
        if (linha.contains(QUESTÃO)) {
            questao.append(QUESTÃO, Long.valueOf(
                    linha.split(QUESTÃO)[1].replace(" ", "")
                            .substring(0, 2)
            ));
        } else
            questao.append(QUESTÃO, Long.valueOf(linha.split(QUESTION)[1].replace(" ", "")
                    .substring(0, 2)));
    }

    @SuppressWarnings("unchecked")
    private Document trataAlternativas(String linha, Document query) {
        if (!query.containsKey(ALTERNATIVAS))
            iniciaAlternativas(query);
        if (linha.replace(" ", "").isEmpty())
            return query;
        query.get(ALTERNATIVAS, ArrayList.class).add(linha);
        return query;
    }

    @SuppressWarnings("unchecked")
    private void iniciaAlternativas(Document query) {
        query.put(ALTERNATIVAS, new ArrayList<Document>());
    }

    private Document trataConteudoPergunta(String linha, Document query) {
        if (!query.containsKey(CONTEUDO))
            query.append(CONTEUDO, new StringBuilder());

        query.get(CONTEUDO, StringBuilder.class).append(" ").append(linha);
        return query;
    }

    private boolean ehAlternativa(String linha) {
        return linha.startsWith(A) || linha.startsWith(B) || linha.startsWith(C) ||
                linha.startsWith(D) || linha.startsWith(E);
    }

    private String retornaPath(String pathProvaAtual, boolean erro) {
        String pasta = (erro ? "/erros/" : "/corretas/");
        try {
            if (!new File(pathProvaAtual + pasta).exists())
                Files.createDirectory(Paths.get(pathProvaAtual + pasta));
            return pathProvaAtual + pasta;

        } catch (IOException i) {
            System.out.println("Algo de errado ocorreu ao gerar pasta de " + erro);
        }

        return null;
    }


    private void loadBlacklist(String pathBlacklist) {
        try {
            blacklist = Files.readAllLines(Paths.get(pathBlacklist));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
