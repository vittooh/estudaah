package br.com.vhcs.estudaah;

import java.util.List;

/**
 * Created by vitor on 26/02/18
 */
public interface Parser {

    void processaObjetivas(List<String> conteudo,String nomeProva) throws Exception;
}
