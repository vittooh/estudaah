package br.com.vhcs.estudaah;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

/**
 * Created by vitor on 20/02/18
 */
public class CapturaProvas {
    private static final String ERRO_NÃO_CONSEGUI_CONECTAR_AO_PROCESSO = "Erro!! não consegui conectar ao processo ";
    private static final String H_1 = "h1";
    private static final String CONTENT = "content";
    private static final String A = "a";
    private static final String PEGANDO_PROVAS_EM = "Pegando provas em ";
    private static final String PDF = ".pdf";
    private static final String HREF = "href";
    private static final String PROVA = "prova";
    private static final String OBJETIVAS = "objetivas";
    private static final String GABARITO = "gabarito";
    private static final String TENTANDO_BAIXAR_ARQUIVO = "Tentando baixar arquivo ";
    private static final String BAIXEI_A_PROVA = "Baixei a prova ";
    private static final String ERRO_OU_AO_ABRIR_OU_FECHAR_INPUT_STREAM_AO_BAIXAR_ARQUIVO = "erro ou ao abrir ou fechar input Stream ao baixar arquivo";
    private static final String ERRO_AO_ESPERAR_TIMEOUT_DA_THREAD = "erro ao esperar timeout da thread!!";
    private static final String BARRA = "/";
    private static String baseURl = "https://www.portalselecao.ufu.br";
    String dirBaseToSave = "/tmp/";

    public void get(String pathFile) throws IOException {

        try (Stream<String> lines = Files.lines(Paths.get(pathFile))) {
            lines.forEach(processo -> {
                Connection c = Jsoup.connect(baseURl + processo);
                try {
                    Document arquivos = c.get();
                    if (c.response().statusCode() != 200) {
                        System.out.println(ERRO_NÃO_CONSEGUI_CONECTAR_AO_PROCESSO + processo);
                    } else {
                        String tituloArquivo = retornaTituloProcesso(arquivos);
                        String nomePasta = tituloArquivo.substring(29, 35);
                        Elements dados = retornaDadosDocument(arquivos);

                        if (criaPasta(nomePasta)) {
                            dados.forEach(file -> {
                                System.out.println(PEGANDO_PROVAS_EM + tituloArquivo);
                                String nomeArquivo = tituloArquivo + " - " + file.text() + PDF;
                                String url = baseURl + file.getAllElements().attr(HREF);

                                try {
                                    if (podeBaixarArquivo(file.text().toLowerCase())) {
                                        salvaProva(url, nomeArquivo, nomePasta);
                                        Thread.sleep(1000);
                                    }
                                } catch (InterruptedException e) {
                                    System.out.println(ERRO_AO_ESPERAR_TIMEOUT_DA_THREAD);
                                }

                            });
                        }
                    }
                } catch (IOException e) {
                    System.out.println("Erro ao tentar abrir conexão com jsoup no processo " + processo);
                    e.printStackTrace();
                }
            });
        }
    }

    private String retornaTituloProcesso(Document arquivos) {
        return arquivos
                .getElementsByTag(H_1)
                .get(arquivos.getElementsByTag(H_1).size() - 1)
                .text();
    }

    private Elements retornaDadosDocument(Document arquivos) {
        return arquivos.body().getElementById(CONTENT).getElementsByTag(A);
    }

    private boolean criaPasta(String nomePasta) {
        return new File(dirBaseToSave + BARRA + nomePasta).mkdir();
    }

    private void salvaProva(String url, String tituloArquivo, String destino) {
        URL urlParsed = null;
        try {
            urlParsed = new URL(url);
            InputStream in = urlParsed.openStream();
            System.out.println(TENTANDO_BAIXAR_ARQUIVO + tituloArquivo);
            Files.copy(in, Paths.get(dirBaseToSave + BARRA + destino + BARRA + tituloArquivo), StandardCopyOption.REPLACE_EXISTING);
            in.close();
            System.out.println(BAIXEI_A_PROVA + tituloArquivo);
        } catch (IOException e) {
            System.out.println(ERRO_OU_AO_ABRIR_OU_FECHAR_INPUT_STREAM_AO_BAIXAR_ARQUIVO);
            e.printStackTrace();
        }
    }

    private boolean podeBaixarArquivo(String arquivoAtual) {
        return (arquivoAtual.contains(PROVA) && arquivoAtual.contains(OBJETIVAS));
    }


}
