import br.com.vhcs.estudaah.Main;
import br.com.vhcs.estudaah.singleton.init.InitializeSingletons;
import br.com.vhcs.estudaah.singleton.mongo.MongoProducer;
import org.bson.Document;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by vitor on 20/03/19
 */
public class InsertProvasMongo {

    @Test
    public void insere() {

        InitializeSingletons.init("/tmp/configMongo.json");

        try (BufferedReader br =
                     new BufferedReader(new InputStreamReader
                             (getClass().getResourceAsStream("testeFireGold.json")))) {

            String provas = br.lines().collect(Collectors.joining("\n"));

            Document paraInserir = Document.parse(provas);

            List<Document> d = (List<Document>) paraInserir.get("provas");

            d.forEach(it -> MongoProducer.retornaInstanciaProvas().insertOne(it));

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void run100TimesBuildIndex() {
        InitializeSingletons.init("/tmp/configMongo.json");

        for (int i = 0; i < 1; i++) {
            MongoProducer.retornaInstanciaIndex().deleteMany(new Document());
            MongoProducer.retornaInstanciaProvas().deleteMany(new Document());
            insere();
            Main.main((String[]) Arrays.asList("/tmp/configMongo.json", "0").toArray());
        }
    }
}
