import br.com.vhcs.estudaah.entidades.DocumentoIndex;
import br.com.vhcs.estudaah.processor.GeraSimilaridade;
import br.com.vhcs.estudaah.processor.ProcessaVocabularioQuery;
import br.com.vhcs.estudaah.singleton.init.InitializeSingletons;
import org.bson.Document;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class TestPrecisao {

    @Before
    public void init() {
        new BuildJsonMongoConfig()
                .salvaJson();

        InitializeSingletons.init("/tmp/configMongo.json");

    }


    @Test
    public void processaTermo() {
        List<DocumentoIndex> docs = ProcessaVocabularioQuery
                .gera(new Document("query", "NOVOS RESIDENTES").append("_id", System.currentTimeMillis()));

        //sim esta maior que 0 e 1 que que ta acontecendo!!!
        System.out.println("Ponderação QUERY ::");
        System.out.println("-------------------------------------------------------------");

        docs.forEach(System.out::println);
        System.out.println("-------------------------------------------------------------");


        System.out.println("RANKING");
        System.out.println("-------------------------------------------------------------");
        AtomicInteger count = new AtomicInteger(1);
        new GeraSimilaridade(docs).call()
                .forEach((k, v) -> {
                    System.out.println(String.format("Ranking :: %d , ID  : %s  , SIMILARIDADE :  %.2f", count.getAndIncrement(), k, v));
                });
        System.out.println("-------------------------------------------------------------");

    }
}
