import br.com.vhcs.estudaah.entidades.MongoConfiguracao;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by vitor on 12/06/18
 */
public class BuildJsonMongoConfig {

    @Test
    public void salvaJson() {
        MongoConfiguracao mongoConfiguracao = new MongoConfiguracao("search-provas-ufu", "localhost",
                27017, null, null, "index");

        Gson gson = new GsonBuilder().setPrettyPrinting().create();


        try (Writer writer = new FileWriter("/tmp/configMongo.json")) {
            gson.toJson(mongoConfiguracao, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
