import br.com.vhcs.estudaah.entidades.DocumentoIndex;
import br.com.vhcs.estudaah.processor.GeraSimilaridade;
import br.com.vhcs.estudaah.processor.ProcessaVocabularioQuery;
import br.com.vhcs.estudaah.singleton.init.InitializeSingletons;
import org.bson.Document;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

/**
 * Created by vitor on 27/03/19
 */
public class TestConsultas {


    @Test
    public void runQueryTodo100Times() throws IOException {
        new BuildJsonMongoConfig()
                .salvaJson();

        InitializeSingletons.init("/tmp/configMongo.json");


        List<DocumentoIndex> docs = ProcessaVocabularioQuery
                .gera(new Document("query", "to do").append("_id", System.currentTimeMillis()));

        Collections.singletonList(new GeraSimilaridade(docs).call().toString()).forEach(System.out::println);


    }
}
