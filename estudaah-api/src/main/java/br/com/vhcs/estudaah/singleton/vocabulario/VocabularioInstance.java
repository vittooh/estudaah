package br.com.vhcs.estudaah.singleton.vocabulario;

import br.com.vhcs.estudaah.entidades.DocumentoIndex;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by vitor on 12/06/18
 */
public class VocabularioInstance {

    public static ConcurrentHashMap<String, ConcurrentHashMap<String, DocumentoIndex>>
            vocabulario;


    public static void init() {
        vocabulario = new ConcurrentHashMap<>();
    }

    public synchronized static void insereItem(String termo, String id) {
        ConcurrentHashMap<String, DocumentoIndex> current;
        if (vocabulario.containsKey(termo)) {
            current = vocabulario.get(termo);
            if (current.containsKey(id)) {
                DocumentoIndex doc = current.get(id);
                doc.setQt(doc.getQt() + 1L);
                current.put(id, doc);
            } else {
                current.put(id, new DocumentoIndex(id, 1L));
            }
        } else {
            current = new ConcurrentHashMap<>();
            current.put(id, new DocumentoIndex(id, 1L));
        }
        vocabulario.put(termo, current);
    }


}
