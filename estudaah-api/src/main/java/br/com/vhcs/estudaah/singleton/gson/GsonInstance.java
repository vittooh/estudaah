package br.com.vhcs.estudaah.singleton.gson;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by vitor on 12/06/18
 */
public class GsonInstance {

    private static Gson gson;

    public static Gson getInstance() {
        synchronized (GsonInstance.class) {
            if (gson == null) {
                gson = new GsonBuilder().setPrettyPrinting().create();
            }
        }
        return gson;
    }
}
