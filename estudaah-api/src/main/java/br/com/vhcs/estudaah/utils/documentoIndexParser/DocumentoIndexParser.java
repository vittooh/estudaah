package br.com.vhcs.estudaah.utils.documentoIndexParser;

import br.com.vhcs.estudaah.entidades.DocumentoIndex;
import org.bson.Document;

public class DocumentoIndexParser {

    private static final String ID = "_id";
    private static final String QT = "QT";
    private static final String TF = "TF";
    private static final String TF_IDF = "TF-IDF";

    public static DocumentoIndex paraModelo(Document mongo) {
        DocumentoIndex documento = new DocumentoIndex();
        documento.set_id(mongo.get(ID).toString());
        documento.setQt(mongo.getLong(QT));
        documento.setTf(mongo.getDouble(TF));
        documento.setTf_idf(mongo.getDouble(TF_IDF));
        return documento;
    }


    public static Document paraMongo(DocumentoIndex documento) {
        return new Document(ID, documento.get_id())
                .append(TF, documento.getTf())
                .append(QT, documento.getQt())
                .append(TF_IDF, documento.getTf_idf());
    }
}
