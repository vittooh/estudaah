package br.com.vhcs.estudaah.processor;

import br.com.vhcs.estudaah.singleton.mongo.MongoProducer;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.Arrays;

/**
 * Created by vitor on 16/10/18
 */
public class GeraNorma {

    public static void calculaNorma() {
        Document unwind = new Document("$unwind", "$DOCS");
        Document group = new Document("$group",
                new Document("_id", new Document("DOC", "$DOCS._id"))
                        .append("NORMA", new Document("$sum",
                                new Document("$pow", Arrays.asList("$DOCS.TF-IDF", 2)))));
        Document project = new Document("$project",
                new Document("NORMA", new Document("$sqrt", "$NORMA"))
                        .append("ID", "$_id.DOC")
                        .append("_id", 0));

        for (Document res : MongoProducer.retornaInstanciaIndex()
                .aggregate(Arrays.asList(unwind, group, project))) {
            MongoProducer.retornaInstanciaProvas()
                    .updateOne(new Document("_id", new ObjectId(res.getString("ID"))),
                            new Document("$set", new Document("NORMA", res.getDouble("NORMA"))));
        }

        System.out.println("Normas atualizadas");
    }
}
