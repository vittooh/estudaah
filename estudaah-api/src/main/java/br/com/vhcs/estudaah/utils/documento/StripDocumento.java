package br.com.vhcs.estudaah.utils.documento;

import org.bson.Document;

import java.util.List;

/**
 * Created by vitor on 12/06/18
 */
public class StripDocumento {

    private static final String ID = "_id";
    private static final String PROVA = "PROVA";
    private static final String QUESTÃO = "QUESTÃO";
    private static final String AREA = "AREA";

    public static Document retiraCamposProcessamento(Document paraProcessar) {
        paraProcessar.remove(ID);
        paraProcessar.remove(PROVA);
        paraProcessar.remove(QUESTÃO);
        paraProcessar.remove(AREA);
        //todo remover apenas para testes
        paraProcessar.remove("alias");
        return paraProcessar;
    }

    public static String[] retornaPalavras(Object it) {
        if (it instanceof String)
            return ((String) it).split(" ");
        else if (it instanceof List) {
            return parseListToArrayString(it);
        }
        return new String[0];
    }

    private static String[] parseListToArrayString(Object it) {
        StringBuilder trataArray = new StringBuilder();
        List<String> v = (List<String>) it;
        for (String s : v) {
            trataArray.append(" ").append(s);
        }

        return trataArray.toString().split(" ");
    }
}
