package br.com.vhcs.estudaah;

import br.com.vhcs.estudaah.entidades.DocumentoIndex;
import br.com.vhcs.estudaah.processor.GeraSimilaridade;
import br.com.vhcs.estudaah.processor.ProcessaProvas;
import br.com.vhcs.estudaah.processor.ProcessaVocabularioQuery;
import br.com.vhcs.estudaah.singleton.init.InitializeSingletons;
import org.bson.Document;

import java.util.List;

public class Main {


    public static void main(String[] args) {
        InitializeSingletons.init(args[0]);

        //0 == processarProvas
        //1 == query
        if (args[1].equals("0")) {
            new ProcessaProvas().run();
        } else {
            List<DocumentoIndex> docs = ProcessaVocabularioQuery
                    .gera(new Document("query", args[2]).append("_id", System.currentTimeMillis()));
            new GeraSimilaridade(docs).call().forEach((k,v) ->{
                System.out.println("id doc " + k + " value " +  v);
            });
        }
    }
}
