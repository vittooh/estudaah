package br.com.vhcs.estudaah.singleton.init;

import br.com.vhcs.estudaah.singleton.config.MongoConfigSingleton;
import br.com.vhcs.estudaah.singleton.mongo.MongoProducer;
import br.com.vhcs.estudaah.singleton.vocabulario.VocabularioInstance;

/**
 * Created by vitor on 20/03/19
 */
public class InitializeSingletons {

    public static void init(String pathConfig) {
        MongoConfigSingleton.init(pathConfig);
        MongoProducer.initMongo();
        VocabularioInstance.init();
    }
}
