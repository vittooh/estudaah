package br.com.vhcs.estudaah.entidades;

/**
 * Created by vitor on 12/06/18
 */
public class MongoConfiguracao extends Configuracao {

    private String collection;

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public MongoConfiguracao(String collection) {
        this.collection = collection;
    }

    public MongoConfiguracao(String banco, String host, Integer porta, String usuario, String senha, String collection) {
        super(banco, host, porta, usuario, senha);
        this.collection = collection;
    }
}
