package br.com.vhcs.estudaah.entidades;

import java.io.Serializable;

/**
 * Created by vitor on 12/06/18
 */
public class DocumentoIndex implements Serializable {

    private String _id;

    private Double tf;

    private Double tf_idf;

    private Double norma;

    private Long qt;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Double getTf() {
        return tf;
    }

    public void setTf(Double tf) {
        this.tf = tf;
    }

    public Double getTf_idf() {
        return tf_idf;
    }

    public void setTf_idf(Double tf_idf) {
        this.tf_idf = tf_idf;
    }

    public Long getQt() {
        return qt;
    }

    public void setQt(Long qt) {
        this.qt = qt;
    }

    @Override
    public String toString() {
        return String.format("_id : %s | QT : %s | TF  : %f | TF-IDF : %f ", _id, qt, tf, tf_idf);
    }

    public DocumentoIndex() {

    }

    public DocumentoIndex(String _id, Long qt) {
        this._id = _id;
        this.qt = qt;
    }

    public Double getNorma() {
        return norma;
    }

    public void setNorma(Double norma) {
        this.norma = norma;
    }

}
