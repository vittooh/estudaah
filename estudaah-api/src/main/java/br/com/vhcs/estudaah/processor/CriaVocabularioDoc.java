package br.com.vhcs.estudaah.processor;

import br.com.vhcs.estudaah.singleton.vocabulario.VocabularioInstance;
import br.com.vhcs.estudaah.utils.documento.StripDocumento;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;

/**
 * Created by vitor on 12/06/18
 */
public class CriaVocabularioDoc implements Runnable {

    private Document paraProcessar;
    private CountDownLatch countDownLatch;

    public CriaVocabularioDoc(Document documentoIndex, CountDownLatch countDownLatch) {
        this.paraProcessar = documentoIndex;
        this.countDownLatch = countDownLatch;
    }

    private List<String> enunciados = Arrays.asList("A)", "B)", "C)", "D)",
            "D1", "D2", "D3", "D4"
    );


    @Override
    public void run() {
        String id = paraProcessar.get("_id").toString();
        StripDocumento.retiraCamposProcessamento(paraProcessar);
        paraProcessar.values().forEach(it -> {
            String[] words = StripDocumento.retornaPalavras(it);
            for (String palavra : words) {
                palavra = StringUtils.stripAccents(palavra);
                palavra = palavra.toUpperCase().replaceAll("[^A-Z]", "");
                System.out.println(palavra);
                if (palavra.trim().length() > 2) {
                    if (enunciados.contains(palavra) || Objects.equals(palavra, "")) continue;
                    VocabularioInstance.insereItem(palavra, id);
                }
            }
        });
        countDownLatch.countDown();
    }
}
