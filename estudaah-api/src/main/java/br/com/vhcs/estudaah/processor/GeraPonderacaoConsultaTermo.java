package br.com.vhcs.estudaah.processor;


import br.com.vhcs.estudaah.entidades.DocumentoIndex;
import br.com.vhcs.estudaah.singleton.mongo.MongoProducer;
import org.bson.Document;

import java.util.concurrent.Callable;

public class GeraPonderacaoConsultaTermo implements Callable<DocumentoIndex> {

    private DocumentoIndex termoQuery;

    private String termo;


    public GeraPonderacaoConsultaTermo(DocumentoIndex termoQuery, String termo) {
        this.termoQuery = termoQuery;
        this.termo = termo;
    }

    private double retornaIDFColecao(String termo) {
        Document query = new Document("TERMO", termo);
        Document resultado = MongoProducer.retornaInstanciaIndex().find(query).first();
        return (resultado == null) ? 0.0 : resultado.getDouble("IDF");
    }

    @Override
    public DocumentoIndex call() {
        termoQuery.setTf(1 + (Math.log10(termoQuery.getQt()) / Math.log10(2)));
        termoQuery.setTf_idf(termoQuery.getTf() * (retornaIDFColecao(termo)));
        termoQuery.set_id(termo);
        return termoQuery;
    }
}
