package br.com.vhcs.estudaah.entidades;

import java.util.Objects;

/**
 * Created by vitor on 12/06/18
 */
public class Configuracao {

    private String banco;

    private String host;

    private Integer porta;

    private String usuario;

    private String senha;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPorta() {
        return porta;
    }

    public void setPorta(Integer porta) {
        this.porta = porta;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public Configuracao() {
    }

    public Configuracao(String banco, String host, Integer porta, String usuario, String senha) {
        this.banco = banco;
        this.host = host;
        this.porta = porta;
        this.usuario = usuario;
        this.senha = senha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Configuracao that = (Configuracao) o;
        return Objects.equals(host, that.host) &&
                Objects.equals(porta, that.porta) &&
                Objects.equals(usuario, that.usuario) &&
                Objects.equals(senha, that.senha);
    }

    @Override
    public int hashCode() {

        return Objects.hash(host, porta, usuario, senha);
    }
}
