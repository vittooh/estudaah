package br.com.vhcs.estudaah.processor;

import br.com.vhcs.estudaah.entidades.DocumentoIndex;
import br.com.vhcs.estudaah.singleton.mongo.MongoProducer;
import br.com.vhcs.estudaah.utils.documentoIndexParser.DocumentoIndexParser;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

/**
 * Created by vitor on 19/06/18
 */
public class GeraPonderacoesTermo implements Runnable {

    private String termo;
    private ConcurrentHashMap<String, DocumentoIndex> docs;
    private Long finalQtDocumentos;

    public GeraPonderacoesTermo(String termo, ConcurrentHashMap<String, DocumentoIndex> docs, Long finalQtDocumentos) {
        this.termo = termo;
        this.docs = docs;
        this.finalQtDocumentos = finalQtDocumentos;
    }

    @Override
    public void run() {
        List<Document> documentosIndex = new ArrayList<>();
        //final docs = 4
        double idf = (Math.log10((double) finalQtDocumentos / docs.size()) / Math.log10(2));
        docs.forEach((id, doc) -> {
            doc.setTf(1 + (Math.log10(doc.getQt()) / Math.log10(2)));
            doc.setTf_idf(doc.getTf() * idf);
            documentosIndex.add(DocumentoIndexParser.paraMongo(doc));
        });


        MongoProducer.retornaInstanciaIndex()
                .insertOne(new Document("TERMO", termo).append("DOCS", documentosIndex).append("IDF", idf));

    }
}
