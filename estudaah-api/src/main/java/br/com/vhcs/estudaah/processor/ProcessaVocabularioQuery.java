package br.com.vhcs.estudaah.processor;

import br.com.vhcs.estudaah.entidades.DocumentoIndex;
import br.com.vhcs.estudaah.singleton.vocabulario.VocabularioInstance;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ProcessaVocabularioQuery {

    public static List<DocumentoIndex> gera(Document query) {
        VocabularioInstance.init();

        CountDownLatch countDown = new CountDownLatch(1);

        CriaVocabularioDoc p = new CriaVocabularioDoc(query, countDown);

        p.run();

        try {
            countDown.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ExecutorService executorService = Executors.newFixedThreadPool(VocabularioInstance.vocabulario.size());

        List<Future> termoQueries = new ArrayList<>();

        VocabularioInstance.vocabulario.forEach((k, v) -> {
            System.out.println("k : " + k);
            v.forEach((key, value) -> termoQueries.add(executorService.submit(new GeraPonderacaoConsultaTermo(value, k))));
        });

        List<DocumentoIndex> termosConsultaPonderados = new ArrayList<>();
        termoQueries.forEach(it -> {
            try {
                termosConsultaPonderados.add((DocumentoIndex) it.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });

        executorService.shutdown();

        return termosConsultaPonderados;

    }

}
