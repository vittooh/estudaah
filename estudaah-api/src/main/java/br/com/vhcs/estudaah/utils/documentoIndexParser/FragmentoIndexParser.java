package br.com.vhcs.estudaah.utils.documentoIndexParser;

import br.com.vhcs.estudaah.entidades.FragmentoIndex;
import org.bson.Document;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by vitor on 16/10/18
 */
public class FragmentoIndexParser {

    public static FragmentoIndex paraModelo(Document fragmentoIndexMongo) {
        FragmentoIndex fragmentoIndex = new FragmentoIndex();
        List<Document> docs = (List<Document>) fragmentoIndexMongo.get("DOCS");

        fragmentoIndex.setDocs(
                docs.stream().map(DocumentoIndexParser::paraModelo).collect(Collectors.toList())
        );

        fragmentoIndex.setTermo((String) fragmentoIndexMongo.get("TERMO"));
        fragmentoIndex.setIdf(fragmentoIndexMongo.getDouble("IDF"));
        return fragmentoIndex;
    }


    public static Document paraMongo(FragmentoIndex fragmentoIndex) {
        Document beanToMongo = new Document();
        beanToMongo.put("DOCS", fragmentoIndex.getDocs().stream().map(DocumentoIndexParser::paraMongo)
                .collect(Collectors.toList()));

        beanToMongo.put("TERMO", fragmentoIndex.getTermo());
        beanToMongo.put("IDF", fragmentoIndex.getIdf());
        return beanToMongo;
    }
}
