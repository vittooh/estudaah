package br.com.vhcs.estudaah.singleton.mongo;

import br.com.vhcs.estudaah.entidades.MongoConfiguracao;
import br.com.vhcs.estudaah.singleton.config.MongoConfigSingleton;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

/**
 * Created by vitor on 12/06/18
 */
public class MongoProducer {

    private static MongoClient mc;
    private static MongoCollection<Document> index;
    private static MongoCollection<Document> provas;

    public static void initMongo() {
        MongoConfiguracao configuracao = MongoConfigSingleton.getInstance();
   /*     mc = new MongoClient(new ServerAddress(configuracao.getHost(), configuracao.getPorta()),
                Collections.singletonList(MongoCredential.createCredential
                        (configuracao.getUsuario(), configuracao.getBanco(), configuracao.getSenha().toCharArray())));
*/
        mc = new MongoClient(configuracao.getHost(), configuracao.getPorta());
        index = mc.getDatabase(configuracao.getBanco()).getCollection("index-estudaah");
        provas = mc.getDatabase(configuracao.getBanco()).getCollection("provas");
    }

    public static MongoCollection<Document> retornaInstanciaIndex() {
        return index;
    }

    public static MongoCollection<Document> retornaInstanciaProvas() {
        return provas;
    }
}
