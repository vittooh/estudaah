package br.com.vhcs.estudaah.processor;

import br.com.vhcs.estudaah.singleton.mongo.MongoProducer;
import br.com.vhcs.estudaah.singleton.vocabulario.VocabularioInstance;
import com.mongodb.client.MongoCursor;
import org.bson.Document;

import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by vitor on 20/03/19
 */
public class ProcessaProvas implements Runnable {


    @Override
    public void run() {
        Long qtProvas = MongoProducer.retornaInstanciaProvas().count();

        if (qtProvas == 0L) {
            System.out.println("Não achei provas para serem processadas!!");
            return;
        }

        ExecutorService executorService = Executors.newFixedThreadPool(qtProvas.intValue());

        MongoCursor<Document> documentos = MongoProducer.retornaInstanciaProvas().find().iterator();

        CountDownLatch countDown = new CountDownLatch(qtProvas.intValue());

        Date now = new Date();

        while (documentos.hasNext()) {
            Document current = documentos.next();
            executorService.submit(new CriaVocabularioDoc(current, countDown));
        }

        try {
            countDown.await();
        } catch (InterruptedException e) {
            System.out.println("Ocorreu um erro ao gerar a ponderação TF, termimando processo!!");
            executorService.shutdown();
            return;
        }

        System.out.println("tempo gasto para gerar tf = " + TimeUnit.MILLISECONDS.toSeconds(new Date().getTime() - now.getTime()));

       // CountDownLatch countDownLatch = new CountDownLatch(VocabularioInstance.vocabulario.size());

        VocabularioInstance.vocabulario.forEach  ((termo, docs) -> new GeraPonderacoesTermo(termo, docs, qtProvas).run());

        GeraNorma.calculaNorma();

        executorService.shutdown();

    }
}
