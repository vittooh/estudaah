package br.com.vhcs.estudaah.entidades;

import java.util.List;

/**
 * Created by vitor on 16/10/18
 */
public class FragmentoIndex {

    private List<DocumentoIndex> docs;

    private String termo;

    private double idf;

    public FragmentoIndex() {
    }

    public List<DocumentoIndex> getDocs() {
        return docs;
    }

    public void setDocs(List<DocumentoIndex> docs) {
        this.docs = docs;
    }

    public String getTermo() {
        return termo;
    }

    public void setTermo(String termo) {
        this.termo = termo;
    }

    public double getIdf() {
        return idf;
    }

    public void setIdf(double idf) {
        this.idf = idf;
    }
}
