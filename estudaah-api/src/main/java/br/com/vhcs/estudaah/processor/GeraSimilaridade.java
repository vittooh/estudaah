package br.com.vhcs.estudaah.processor;

import br.com.vhcs.estudaah.entidades.DocumentoIndex;
import br.com.vhcs.estudaah.entidades.FragmentoIndex;
import br.com.vhcs.estudaah.singleton.mongo.MongoProducer;
import br.com.vhcs.estudaah.utils.documentoIndexParser.FragmentoIndexParser;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by vitor on 16/10/18
 */
public class GeraSimilaridade implements Callable<HashMap<String, Double>> {

    public static final String NORMA = "NORMA";
    private ConcurrentHashMap<String, Double> similaridade;

    private List<DocumentoIndex> docsQuery;

    private HashMap<String, Double> normaCache;

    private double normaQuery = 0.0;

    public GeraSimilaridade(List<DocumentoIndex> docsQuery) {
        this.similaridade = new ConcurrentHashMap<>();
        this.docsQuery = docsQuery;
        this.normaCache = new HashMap<>();
    }

    private FragmentoIndex getByTermoIndex(String termo) {
        Document d = new Document("TERMO", termo);
        return FragmentoIndexParser
                .paraModelo(MongoProducer.retornaInstanciaIndex()
                        .find(d)
                        .first());
    }

    private Double getByIDDoc(String id) {
        if (!normaCache.containsKey(id)) {
            Document current =
                    MongoProducer.retornaInstanciaProvas()
                            .find(new Document("_id", new ObjectId(id)))
                            .first();
            normaCache.put(id, current.getDouble(NORMA));
        }
        return normaCache.get(id);
    }

    @Override
    public HashMap<String, Double> call() {
        docsQuery.forEach(it -> {
                    normaQuery += Math.pow(it.getTf_idf(), 2);
                    getByTermoIndex(it.get_id())
                            .getDocs()
                            .forEach(doc -> {
                                        if (similaridade.containsKey(doc.get_id())) {
                                            Double simiLaridadeCalculada = similaridade.get(doc.get_id());
                                            similaridade.
                                                    put(doc.get_id(),
                                                            simiLaridadeCalculada + (doc.getTf_idf() * it.getTf_idf()));
                                        } else {
                                            similaridade.
                                                    put(doc.get_id(), doc.getTf_idf() * it.getTf_idf());
                                        }
                                    }
                            );
                }
        );

        normaQuery = Math.sqrt(normaQuery);

        similaridade.forEach((key, value) -> {
            Double normaDoc = getByIDDoc(key);
            value = value / (normaDoc * normaQuery);
            similaridade.put(key, value);
        });

        return similaridade.entrySet()
                .stream()
                .sorted((o1, o2) -> {
                    if (o1.getValue() >= o2.getValue()) {
                        return -1;
                    } else
                        return 0;
                })
                .collect(
                        Collectors.toMap
                                (Map.Entry::getKey, Map.Entry::getValue,
                                        (e1, e2) -> e1, LinkedHashMap::new)
                );
    }
}
