package br.com.vhcs.estudaah.singleton.config;

import br.com.vhcs.estudaah.entidades.MongoConfiguracao;
import br.com.vhcs.estudaah.singleton.gson.GsonInstance;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by vitor on 12/06/18
 */
public class MongoConfigSingleton {
    private static MongoConfiguracao mongoConfiguracao;

    public static void init(String path) {
        try {
            JsonReader jsonReader = new JsonReader(new FileReader(new File(path)));
            mongoConfiguracao = GsonInstance.getInstance()
                    .fromJson(jsonReader, MongoConfiguracao.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static MongoConfiguracao getInstance() {
        return mongoConfiguracao;
    }
}
